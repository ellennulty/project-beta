# CarCar
![logo](logo1.png)
---
CarCar is full stack development project that uses Python, Django, PostgreSQL, JSON, Javascript, ReactJS, HTML, CSS and Bootstrap.
This application is designed to be for automobile dealership management and allowing a dealership to track sales, service appointment, employees and more.

## Team:
* Ellen Nulty - Service microservice
* Taylor Harris - Sales microservice

## How to open the application
Please for and close this repository to your local machine.
Once you have done so, please run the following commands to start the containers to properly run the servers
- docker volume create beta-data
- docker-compose build
- docker-compose up
this will create and start all the docker containers to be able to run this application.
If you follows the steps you should be able to go to [localhost3000](http://localhost:3000/) within your browser.


## Design
![diagram](diagram.png)


## Service microservice
The service microservice is a Django based application that provides the back end endpoints for information regarding technicians and appointments. This microservice has a Technician and Appointment model and polls from the inventory microservice to create an AutomobileVO model. For the models, there are view functions for displaying and creating instances; these endpoints can be accessed through API calls in our ReactJS based front end.

### API Endpoints
Once the application is running, Insomnia can be used to access the following endpoints:

| Action | Method | URL |
| ----------- | ----------- | ----------- |
| Get a list of all technicians | GET | http://localhost:8080/api/technicians/ |
| Create a technician | POST | http://localhost:8080/api/technicians/ |
| Delete a technician by ID | DELETE | http://localhost:8080/api/technicians/IDNUMBER |
| Get a list of all appointments | GET | http://localhost:8080/api/appointments/ |
| Create an appointment | POST | http://localhost:8080/api/appointments/ |
| Delete an appointment | DELETE | http://localhost:8080/api/appointments/IDNUMBER |
| Set appointment status to canceled | PUT | http://localhost:8080/api/IDNUMBER/cancel |
| Set appointment status to finished | PUT | http://localhost:8080/api/IDNUMBER/finish |


### POST samples
For each POST endpoint, if the request is okay, Insomnia will return the following. If not, you will receive a 400 error status and a corresponding error message.

- Create a technician
`{
	"id": 1,
	"first_name": "Jane",
	"last_name": "Doe",
	"employee_id": "jdoe"
}`

- Create an appointment
`{
    "date_time": "2023-04-28T11:00",
	"reason": "reason for appointment",
	"vin": "6Y9NIHQCDW3BIW7GW",
	"customer": "John Doe",
	"technician_id": 6
}`

### Services Poller
The services poller polls from the inventory microservice at *http://project-beta-inventory-api-1:8000/api/automobiles/*
This polls a list all automobiles from the inventory microservice and in the services microservice, an AutomobileVO instance is created with the field "vin_vo" that corresponds with "vin" field of each automobile in the inventory microservice.

### Services Models Explained
**AutomobileVO** *this is a value object*
- vin_vo: a polled data point from inventory that is used to uniqly identify a automobile

**Technician**
- first_name: first name of technician
- last_name: last name of technician
- employee_id: emmployee id of a technician
- id: (created automaticly by django) Used to uniqly identify a technician

**Appointment**
- date_time: date and time of appointment
- reason: reason for appointment
- vin: vin (identifying number) for vehicle
- customer: customer name
- is_vip: true or false, was this automobile ever part of the inventory
- technician: a foreign key to technician
- id: (created automaticly by django) Used to uniqly identify a salesperson


## Sales microservice
The sales microservice is a django based back end that creates models, connects to a PostgreSQL, polls from the inventory microservice and created veiw functions that allow the ReactJS based front end to make API calls to create and display data to a front end user.
Sale specifically deals with the orginization of customers, sales people and tracking past sales.

### API Endpoints
Once application is running on your local machine, these are the URLS you can use within insomnia to create and access data.
| Action | Method | URL |
| ----------- | ----------- | ----------- |
| Get a list of all sales people | GET | http://localhost:8090/api/salespeople/ |
| Create a sales person | POST | http://localhost:8090/api/salespeople/ |
| Delete a salesperson by ID | DELETE | http://localhost:8090/api/salespeople/IDNUMBER |
| Get a list of all customers | GET | http://localhost:8090/api/customers/ |
| Create a customer | POST | http://localhost:8090/api/customers/ |
| Delete a customer by ID | DELETE | http://localhost:8090/api/customers/IDNUMBER |
| Get a list of all sales | GET | http://localhost:8090/api/sales/ |
| Create a sale | POST | http://localhost:8090/api/sales/ |

### POST samples
For each of these samples, if the data is formatted correctly, you should receive a it returned in the preview portion of insomina. If there is something wrong with the data you will get a error message either saying could not create or in the case for sales, will return which id/vin is invalid.
If the response is valid and fires properly, you should get a 200 OK

- Create a sales person
`{
  "first_name": "Jane",
  "last_name": "Doe",
  "employee_id": 555
}`

- Create a customer
`{
  "first_name": "John",
  "last_name": "Smit",
  "address": "1st st, Seattle, WA",
    "phone_number": "555-555-5555"
}`

- Create a sale - Please note that automobile vin, salesperson is, and customer is are all foreign keys. These items must already be in the database
`{
  "automobile": "1C3CC5FB2A6120174",
  "salesperson": 1,
  "customer": 1,
	"price": "1200.50"
}`

### Sales Poller
The sales poller is connected to the inventory microservice to poll from *http://project-beta-inventory-api-1:8000/api/automobiles/*
This polls a list of all automobiles in the inventory and grabs the vin and creates a AutomobileVO with the property of vin within the sales microservice.

### Sales Models Explained

**AutomobileVO** *this is a value object*
- vin: a polled data point from inventory that is used to uniquely identify a automobile

**Customer**
- first_name: first name of customer
- last_name: last name of customer
- address: address of customer
- phone_number: phone number of customer
- id: (created automaticly by django) Used to uniquely identify a customer

**Salesperson**
- first_name: first name of salesperson
- last_name: last name of salesperson
- employee_id: a number field that can be used to identify a salesperson
- id: (created automaticly by django) Used to uniquely identify a salesperson

**Sale**
- automobile: a foreign key to AutomobileVO via the vin
- salesperson: a foreign key to Salesperson via the ID
- customer: a foreign key to Custoemr via the ID
- price: price the automobile is sold for
- id: (created automaticly by django) Used to uniquely identify a salesperson
