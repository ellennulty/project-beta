from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder

from django.shortcuts import render
from .models import Technician, AutomobileVO, Appointment


class TechnicianListEncoder(ModelEncoder):
    model= Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id"
    ]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "is_vip",
        "technician"
    ]
    encoders = {
        "technician": TechnicianListEncoder()
    }

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin_vo"
    ]


@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        content = json.loads(request.body)
    try:
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False
        )
    except:
        response = JsonResponse(
            {"message": "Could not create technician"}
        )
        response.status_code = 400
        return response


@require_http_methods(["DELETE"])
def delete_technician(request, pk):
    try:
        count, _= Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    except:
        response = JsonResponse(
            {"message": "Could not delete"}
        )
        response.status_code = 400
        return response


@require_http_methods(["GET", "POST"])
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder
        )
    else:
        content = json.loads(request.body)
    try:
        technician_id = content["technician_id"]
        technician = Technician.objects.get(id=technician_id)
        content["technician"] = technician

        check = AutomobileVO.objects.filter(vin_vo=content["vin"])
        if check.count() > 0:
            content["is_vip"] = True

        else:
            content["is_vip"] = False

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )
    except:
        response = JsonResponse(
            {"message": "Could not make appointment"}
        )
        response.status_code = 400
        return response

@require_http_methods(["DELETE"])
def delete_appointment(request, pk):
    try:
        count, _= Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    except:
        response = JsonResponse(
            {"message": "Could not delete"}
        )
        response.status_code = 400
        return response

@require_http_methods(["PUT"])
def cancel_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.status = "Canceled"
        appointment.save()

        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )
    except:
        response = JsonResponse(
            {"message": "Status was not updated"}
        )
        response.status_code = 400
        return response

@require_http_methods(["PUT"])
def finish_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.status = "Finished"
        appointment.save()

        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )
    except:
        response = JsonResponse(
            {"message": "Status was not updated"}
        )
        response.status_code = 400
        return response

@require_http_methods(["GET"])
def list_automobile_vos(request):
    if request.method == "GET":
        automobile_vos = AutomobileVO.objects.all()
        return JsonResponse(
            {"autombile_vos": automobile_vos},
            encoder=AutomobileVOEncoder
        )
