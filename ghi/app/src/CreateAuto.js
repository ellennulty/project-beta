import React, { useEffect, useState } from 'react'

function CreateAuto({ setAuto }) {
    const [color, setColor] = useState('')
    const [year, setYear] = useState('')
    const [vin, setVin] = useState('')
    const [model_id, setModel] = useState('')
    const [models, setModels] = useState([])

    useEffect(() => {
        async function getModels() {
        const url = 'http://localhost:8100/api/models/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setModels(data.models)

        }
        }
        getModels()
        }, []);

    async function handleSubmit(event) {
        event.preventDefault()
        const data = {
        color,
        year,
        vin,
        model_id: model_id,
        }

        const autoUrl = 'http://localhost:8100/api/automobiles/'
        const options = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            },
        }

        const response = await fetch(autoUrl, options);
            if (response.ok) {
                const newAuto = await response.json();
                setAuto(newAuto);
                setColor('');
                setYear('');
                setVin('');
                setModel('');
                }
        }

        const handleColor = (event) => {
            setColor(event.target.value);
        };

        const handleYear = (event) => {
            setYear(event.target.value);
        };

        const handleVin = (event) => {
            setVin(event.target.value);
        };

        const handleModel = (event) => {
            setModel(event.target.value);
        };



    return (
        <div className='row'>
            <div className='offset-3 col-6'>
                <div className='p-4 mt-4'>
                    <h2>Create a new automobile</h2>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">

                            <input type="text" id="color" value={color} onChange={handleColor} placeholder="color" required type="text" name="color" className="form-control"/>
                            <label htmlFor="color">Color:</label>
                        </div>

                        <div className='form-floating mb-3'>

                            <input type='text' id='year' value={year} onChange={handleYear} placeholder='year' required name='year' className="form-control"/>
                            <label htmlFor='year'>Year:</label>
                            </div>

                        <div className='form-floating mb-3'>

                            <input type='text' id='vin' value={vin} onChange={handleVin} placeholder='vin' required name='vin' className="form-control"/>
                            <label htmlFor='vin'>vin:</label>
                            </div>


                        <div className="form-floating mb-3">

                            <select id="model_id" value={model_id} onChange={handleModel}  multiple={false} name='model_id' className="form-control">
                            <option value="">......</option>
                            {models.map((model) => (

                                <option key={model.id} value={model.id}>
                                {model.name}
                                </option>


                            ))}
                            </select>
                            <label htmlFor="model">Choose a model</label>
                        </div>

                        <button className="btn btn-primary">Create Auto</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default CreateAuto
