import React, { useState, useEffect } from 'react';

function SalesHistory() {
    const [sale, setSale] = useState([]);
    const [salesperson, setSalespeople] = useState([]);
    const [selectedId, setId] = useState('');

    // load all sales
    async function loadSales() {
        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSale(data.sale);
        }
    }

    // load all salespeople
    async function loadSalespeople() {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salesperson);
        }
    }

    useEffect(() => {
        loadSalespeople();
        loadSales();
    }, []);

    // handle salesperson selection
    async function handleSelection(event) {
        const value = event.target.value;
        setId(value);
    }

    const filtered = sale.filter((sale) => sale.salesperson.id === parseInt(selectedId));
    const filteredArray = Object.values(filtered)





    return (
        <div>
            <h1>SalesPerson History</h1>
            <div className="form-floating mb-3">
                <select id="person" value={selectedId} onChange={handleSelection} name="person" className="form-control" >
                    <option>Choose a Salesperson</option>
                    {salesperson.map((person) => (
                        <option key={person.id} value={person.id}>
                            {person.first_name} {person.last_name}
                        </option>
                    ))}
                </select>
                <label htmlFor="model">Salesperson:</label>
            </div>

            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredArray.map(saleZ => (
                            <tr key={saleZ.id}>
                                <td>{saleZ.salesperson.first_name} {saleZ.salesperson.last_name}</td>
                                <td>{saleZ.customer.first_name} {saleZ.customer.last_name}</td>
                                <td>{saleZ.automobile.vin}</td>
                                <td>{saleZ.price}</td>
                            </tr>
                        ))}
                </tbody>
            </table>
        </div>
    );
}

export default SalesHistory;
