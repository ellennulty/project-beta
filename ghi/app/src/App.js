import { BrowserRouter, Routes, Route } from 'react-router-dom';
import CreateManufacturer from './CreateManufacturer';
import ModelsList from './ListModels';
import MainPage from './MainPage';
import ManufacturerList from './ManufacturerList';
import Nav from './Nav';
import CreateAuto from './CreateAuto'
import ShowAuto from './ShowAuto'
import CreateVehicle from './CreateVehicle'
import CreateSalesperson from './CreateSalesperson'
import React, { useEffect, useState } from 'react';
import ListSalespeople from './ListSalespeople'
import CreateCustomer from './CreateCustomer'
import ListCustomers from './ListCustomers'
import CreateSale from './CreateSale'
import ListSales from './ListSales'
import SalesHistory from './SalesHistory'
import TechnicianList from './ListTechnicians';
import AppointmentForm from './CreateAppointment';
import TechnicianForm from './CreateTechnician';
import AppointmentList from './ServiceAppointments';
import ServiceHistory from './ServiceHistory';

function App(props) {

  const [ auto, setAuto ] = useState([]);

  async function getAuto() {
    const url = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAuto(data.auto);
    }
  }

  useEffect(() => {
    getAuto();
  }, []);


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="automobiles/create" element={<CreateAuto setAuto={setAuto}/>} />
          <Route path="automobiles/" element={<ShowAuto />} />
          <Route path="vehicles/create" element={<CreateVehicle />} />
          <Route path="manufacturers" element={<ManufacturerList />} />
          <Route path="manufacturers/new" element={<CreateManufacturer />} />
          <Route path="models" element={<ModelsList models={props.models} />} />
          <Route path="salespeople/create" element={<CreateSalesperson />} />
          <Route path="salespeople/" element={<ListSalespeople />} />
          <Route path="customers/" element={<ListCustomers />} />
          <Route path="customers/create" element={<CreateCustomer />} />
          <Route path="sales/" element={<ListSales />} />
          <Route path="sales/create" element={<CreateSale />} />
          <Route path="saleshistory/" element={<SalesHistory />} />
          <Route path="models" element={<ModelsList />} />
          <Route path="technicians" element={<TechnicianList />} />
          <Route path="technicians/create" element={<TechnicianForm />} />
          <Route path="appointments/create" element={<AppointmentForm />} />
          <Route path="appointments/" element={<AppointmentList />} />
          <Route path="appointments/history" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
