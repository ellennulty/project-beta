from django.shortcuts import render
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .encoders import (SalespersonEncoder,
                    SaleEncoder,
                    CustomerEncoder)

from .models import (AutomobileVO,
                    Sale,
                    Salesperson,
                    Customer)




# views for Customer, one to GET and POST to the list and one to delete a customer by ID
# this view alows you to get the list of customers and make new ones
@require_http_methods(["GET", "POST"])
def customers_list(request):

    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )

    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                {"customer": customer},
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            message = JsonResponse(
                {"message": "Something went wrong, could not create customer."}
            )
            message.status_code = 400
            return message

# this view allows you to get a customer by id and delete them
@require_http_methods(["GET", "DELETE"])
def customer_view(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({"deleted customer": count > 0})




# views for Salesperson, one to GET and POST to the list and one to delete a Salesperson by ID
# this view alows you to get the list of salespersons(salespeople? lol) and make new ones
@require_http_methods(["GET", "POST"])
def salesperson_list(request):
    if request.method == "GET":
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespersonEncoder
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                {"salesperson": salesperson},
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            message = JsonResponse(
                {"message": "Something went wrong, could not create salesperson."}
            )
            message.status_code = 400
            return message

# this view allows you to get a salesperson by id and delete them
@require_http_methods(["GET", "DELETE"])
def salesperson_view(request, pk):
    if request.method == "GET":
        salesperson = Salesperson.objects.get(id=pk)
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespersonEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Salesperson.objects.filter(id=pk).delete()
        return JsonResponse({"deleted salesperson": count > 0})


# these views are for the Sale Model. since Sales had 3 ForeignKeys, these views are signifigantly longer and have room for more error.
# This view allows you to get a list of all sales and create new sales
@require_http_methods(["GET", "POST"])
def sales_list(request):

    if request.method == "GET":
        sale = Sale.objects.all()
        return JsonResponse(
            {"sale": sale},
            encoder=SaleEncoder
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        # try to get customer
        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Customer ID"},
                status=400,
            )
        # try to get salesperson
        try:
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Salesperson ID"},
                status=400,
            )
        # try to get automobile
        try:
            automobile_id = content["automobile"]
            vin = AutomobileVO.objects.get(vin=automobile_id)
            content["automobile"] = vin
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid vin"},
                status=400,
            )
        # if all id's work, create new sale
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )

# this view allows you to grab a sale and either just show it or delete it
@require_http_methods(["GET", "DELETE"])
def sale_view(request, pk):
    if request.method == "GET":
        sale = Sale.objects.get(id=pk)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )
    else:
        count, _ = Sale.objects.filter(id=pk).delete()
        return JsonResponse({"deleted sale": count > 0})
